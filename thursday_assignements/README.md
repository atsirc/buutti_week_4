# Thursday's assignements  
## Part 1. Azure function

link: https://christasfirstazurefunction.azurewebsites.net/api/christasFirstAzureFunction?code=rxpQCPQzwttNLy8DSdiqdhuDJlr3mlpPxfIRBgLv7eVYA3tkc4paZA==  
can also be tested with VS Codes REST client through `test.rest` which can be found in `azure_function`
  
## Part 2. Container or App?
  
It was unclear to me if we should try to get a web app running or create a container... I tried the former and followed [theese instructions](https://docs.microsoft.com/fi-fi/azure/app-service/quickstart-nodejs?tabs=windows&pivots=development-environment-vscode) which worked fine... I was able to update and redeploy through VS Code Azure extension.
  
App: https://christasexpressapp.azurewebsites.net/ (has been deleted)
  
After some thinking I also created a new container. I used an own project and created a container using NGINX becasue the project was written in basic html/js. That worked fine and dandy until I tried to configure the CD part. I could for the life of me not get the webhooks to work (updated spoiler: the webhooks were not the way)... After a while decided to try to use GitHub. Only problem I had was that a file I hadn't wanted to add to GitHub had to be published... Could be an idea to try to figure out how to avoid it though... GitHub repo: (https://github.com/atsirc/helsinki-streets/tree/containerTest)
  
App: https://christas2docker.azurewebsites.net/
  
***Update***: So github worked wonders, only mistake on my part was to give the "wrong" tag-name. Out of curiosity I decided to retry my luck with the basic version of the Azure CD-connection, i.e Deployment Center -> [x] Container Registry: Setup your app... + [x] Continuous deployment. This time I used **az cli** with **docker** instead of the VS Code Docker Azure extension. I followed [theese instructions](https://docs.microsoft.com/en-us/azure/container-registry/container-registry-get-started-docker-cli?tabs=azure-cli). It seems to me that azure updates the container pretty slowly, but when I clicked *Restart* in the containers *Overview* it updated immediately.
  
## Part 3. Communicating functions
  
Took a while for me to understand the question. The second function was easy, but then I had 2 problems. The first was that I wanted to run the test fully locally, so had to change host in local.settings.json

```json
{
    "IsEncrypted": false,
    "Values": {
        "AzureWebJobsStorage": "",
        "FUNCTIONS_WORKER_RUNTIME": "node"
    },
    "Host": {
        "LocalHttpPort": 7073
    }
}
```
and change another port as well, in .vscode/launch.json

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Attach to Node Functions",
            "type": "node",
            "request": "attach",
            "port": 9230,
            "preLaunchTask": "func: host start"
        }
    ]
}
```
in one of the azure function-folders (of course if using more than two, you have to change their values as well, so that there are no conflicts)
  
Another problem I encountered, and for some reason got stuck on was:
```
TypeError: Converting circular structure to JSON --> starting at object with constructor 'ClientRequest'
``` 
which turned out to be a basic mistake from my part. I forgot to use .data for updatedAnimal
  
correct version:
```javascript
const updatedAnimal = await axios.post(url, animal);

context.res = {
    status: 200,
    // yo, .data is important! got an Exception: TypeError: Converting circular structure to JSON!!!
    body: updatedAnimal.data
}
``` 
REST Client POST-tests: `azure_callee/test.http`  
working url: https://christascallerfunction.azurewebsites.net/api/christasCallerFunction?code=4lpcjklftVX/3XSOqZ1zRCaavp1fwwo8fgUo2S7Xh7ihLzyDvh1Dzw==
