module.exports = async function(context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');
    const first = req.body.first;
    const second = req.body.second;
    if (!first || !second) {
        context.res = {
            status: 400,
            body: `bad request with body parameters ${first} and ${second}`
        };
    } else {
        if (Number(first) && Number(second)) {
            sum = first + second;
            context.res = {
                status: 200,
                body: `the sum is ${sum}`
            }
        } else {
            context.res = {
                status: 200,
                body: `${first} and/or ${second} could not be read as numbers`
            }
        }
    }
};